use vulkano::device::physical::{PhysicalDevice, PhysicalDeviceType, QueueFamily};
use vulkano::device::{Device, DeviceExtensions, Features, Queue};
use vulkano::image::swapchain::SwapchainImage;
use vulkano::image::ImageUsage;
use vulkano::instance::Instance;
use vulkano::swapchain::{Surface, Swapchain, SwapchainCreationError};
use vulkano::Version;
use vulkano_win::{self as vkw, VkSurfaceBuild};

use winit::event_loop::EventLoop;
use winit::window::{Window, WindowBuilder};

use std::error::Error;
use std::iter;
use std::sync::Arc;

fn find_physical_for<P, Q>(
    instance: &Arc<Instance>,
    physical_filter: P,
    queue_filter: Q,
) -> Option<(PhysicalDevice, QueueFamily)>
where
    P: Fn(&PhysicalDevice) -> bool,
    Q: Fn(&QueueFamily) -> bool + Copy,
{
    PhysicalDevice::enumerate(instance)
        .filter(physical_filter)
        .filter_map(|p| p.queue_families().find(queue_filter).map(|q| (p, q)))
        .min_by_key(|(p, _)| match p.properties().device_type {
            PhysicalDeviceType::DiscreteGpu => 0,
            PhysicalDeviceType::IntegratedGpu => 1,
            PhysicalDeviceType::VirtualGpu => 2,
            PhysicalDeviceType::Cpu => 3,
            PhysicalDeviceType::Other => 4,
        })
}

fn create_swapchain(
    device: Arc<Device>,
    surface: Arc<Surface<Window>>,
    queue: Arc<Queue>,
) -> Result<(Arc<Swapchain<Window>>, Vec<Arc<SwapchainImage<Window>>>), Box<dyn Error>> {
    let caps = surface.capabilities(device.physical_device())?;
    let dimensions: [u32; 2] = surface.window().inner_size().into();
    let composite_alpha = caps
        .supported_composite_alpha
        .iter()
        .next()
        .ok_or_else(|| String::from("No composite alpha available"))?;
    let num_images = caps
        .max_image_count
        .unwrap_or(u32::MAX)
        .min(caps.min_image_count + 1);
    let (format, _) = caps.supported_formats[0];
    Swapchain::start(device.clone(), surface.clone())
        .dimensions(dimensions)
        .composite_alpha(composite_alpha)
        .num_images(num_images)
        .format(format)
        .usage(ImageUsage::color_attachment())
        .sharing_mode(&queue)
        .build()
}

fn main() -> Result<(), Box<dyn Error>> {
    // instance
    let instance = Instance::new(None, Version::V1_1, &vkw::required_extensions(), None)?;

    // event loop and window surface
    let eventloop = EventLoop::new();
    let surface = WindowBuilder::new().build_vk_surface(&eventloop, instance.clone())?;

    // physical device
    let physical_ext = DeviceExtensions {
        khr_swapchain: true,
        ..DeviceExtensions::none()
    };
    let (physical, family) = find_physical_for(
        &instance,
        |p| p.supported_extensions().is_superset_of(&physical_ext),
        |q| q.supports_graphics() && surface.is_supported(*q).unwrap_or(false),
    )
    .ok_or_else(|| String::from("No supported devices available"))?;

    // device and queue
    let (device, mut queues) = Device::new(
        physical,
        &Features::none(),
        &physical.required_extensions().union(&physical_ext),
        iter::once((family, 0.5)),
    )?;
    let queue = queues
        .next()
        .ok_or_else(|| String::from("No queues available"))?;

    // swapchain
    let caps = surface.capabilities(physical)?;
    let dimensions: [u32; 2] = surface.window().inner_size().into();
    let composite_alpha = caps
        .supported_composite_alpha
        .iter()
        .next()
        .ok_or_else(|| String::from("No composite alpha available"))?;
    let num_images = caps
        .max_image_count
        .unwrap_or(u32::MAX)
        .min(caps.min_image_count + 1);
    let (format, _) = caps.supported_formats[0];
    let swapchain = Swapchain::start(device.clone(), surface.clone())
        .dimensions(dimensions)
        .composite_alpha(composite_alpha)
        .num_images(num_images)
        .format(format)
        .usage(ImageUsage::color_attachment())
        .sharing_mode(&queue)
        .build()?;

    Ok(())
}
