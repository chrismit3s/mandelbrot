use std::iter;
use std::time::{Duration, Instant};
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer};
use vulkano::command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage};
use vulkano::descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::physical::{PhysicalDevice, QueueFamily};
use vulkano::device::{Device, DeviceExtensions, Features};
use vulkano::instance::{Instance, InstanceExtensions};
use vulkano::pipeline::{ComputePipeline, Pipeline, PipelineBindPoint};
use vulkano::sync::{self, GpuFuture};
use vulkano::Version;

pub fn format_duration(d: Duration, n: usize) -> String {
    const UNITS: &'static [(&'static str, u128)] = &[
        ("ns", 1000),
        ("us", 1000),
        ("ms", 1000),
        ("s", 60),
        ("min", 60),
        ("h", u128::MAX),
    ];

    UNITS
        .iter()
        .scan(d.as_nanos(), |remaining, (unit, size)| {
            let value = *remaining % size;
            *remaining /= size;
            Some((value, unit))
        })
        .filter(|&(v, _)| v > 0)
        .collect::<Vec<_>>()
        .into_iter()
        .rev()
        .take(n)
        .map(|(v, u)| format!("{}{}", v, u))
        .reduce(|s, vu| s + " " + &vu)
        .unwrap_or_else(|| String::from("0ns"))
}

mod shadermod {
    vulkano_shaders::shader! {
        ty: "compute",
        src: r#"
        #version 450

        layout(local_size_x = 1024, local_size_y = 1, local_size_z = 1) in;

        layout(set = 0, binding = 0) buffer DataSrc {
            uint data[];
        } srcbuf;

        layout(set = 0, binding = 1) buffer DataDest {
            uint data[];
        } destbuf;

        void main() {
            uint i = gl_GlobalInvocationID.x;
            destbuf.data[i] = srcbuf.data[i];
            srcbuf.data[i] *= srcbuf.data[i];
        }
        "#,
    }
}

const LOCAL_SIZE: u32 = 1024;
const N: u32 = 1 << 24;
const WORK_GROUPS: u32 = N / LOCAL_SIZE;

fn main() {
    // create an instance and get a physical device
    let instance = Instance::new(None, Version::V1_1, &InstanceExtensions::none(), None).unwrap();
    let physical = PhysicalDevice::enumerate(&instance).next().unwrap();
    println!(
        "Created vk instance and got physicaldevice '{}'",
        physical.properties().device_name
    );

    // select a queue family and setup the device handle
    let family = physical
        .queue_families()
        .filter(QueueFamily::supports_graphics)
        .next()
        .unwrap();
    let (device, mut queues) = Device::new(
        physical,
        &Features::none(),
        &DeviceExtensions::none(),
        iter::once((family, 0.5)),
    )
    .unwrap();
    let queue = queues.next().unwrap();
    println!("Created device and queue");

    // create buffers
    let srcbuf =
        CpuAccessibleBuffer::from_iter(device.clone(), BufferUsage::all(), false, 0..N).unwrap();
    let destbuf = CpuAccessibleBuffer::from_iter(
        device.clone(),
        BufferUsage::all(),
        false,
        vec![0u32; N as usize],
    )
    .unwrap();
    println!("Created buffers of size {}", N);

    // create shader and pipeline
    let shader = shadermod::load(device.clone()).unwrap();
    let pipeline = ComputePipeline::new(
        device.clone(),
        shader.entry_point("main").unwrap(),
        &(),
        None,
        |_| {},
    )
    .unwrap();
    println!("Loaded shader and created computepipeline");

    // bind the buffer
    let layout = &pipeline.layout().descriptor_set_layouts()[0];
    let set = PersistentDescriptorSet::new(
        layout.clone(),
        [
            WriteDescriptorSet::buffer(0, srcbuf.clone()),
            WriteDescriptorSet::buffer(1, destbuf.clone()),
        ],
    )
    .unwrap();
    println!("Bound buffers to pipeline");

    // build the command buffer
    let mut cmdbufbuilder = AutoCommandBufferBuilder::primary(
        device.clone(),
        queue.family(),
        CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap();
    cmdbufbuilder
        //.copy_buffer(srcbuf.clone(), destbuf.clone())
        //.unwrap()
        .bind_pipeline_compute(pipeline.clone())
        .bind_descriptor_sets(
            PipelineBindPoint::Compute,
            pipeline.layout().clone(),
            0,
            set,
        )
        .dispatch([WORK_GROUPS, 1, 1])
        .unwrap();
    let cmdbuf = cmdbufbuilder.build().unwrap();
    println!("Built the commandbuffer with {} workgroups", WORK_GROUPS);

    // run the command buffer
    let start = Instant::now();
    sync::now(device.clone())
        .then_execute(queue.clone(), cmdbuf)
        .unwrap()
        .then_signal_fence_and_flush()
        .unwrap()
        .wait(None)
        .unwrap();
    println!(
        "Ran the commandbuffer in {}",
        format_duration(start.elapsed(), 2),
    );

    let srccontent = srcbuf.read().unwrap();
    let destcontent = destbuf.read().unwrap();

    let start = Instant::now();
    let srcmatch = srccontent
        .iter()
        .enumerate()
        .filter(|&(i, &x)| (i * i) as u32 != x)
        .next();
    let destmatch = destcontent
        .iter()
        .enumerate()
        .filter(|&(i, &x)| i as u32 != x)
        .next();
    println!(
        "Checked the results in {}",
        format_duration(start.elapsed(), 2),
    );

    println!(
        "srcbuf: {}",
        srcmatch
            .map(|t| format!("mistake at {:?}", t))
            .unwrap_or(String::from("matches")),
    );
    println!(
        "destbuf: {}",
        destmatch
            .map(|t| format!("mistake at {:?}", t))
            .unwrap_or(String::from("matches")),
    );
}
